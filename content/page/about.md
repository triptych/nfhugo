---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

My name is Andrew Wooldridge. I'm a webdev at [eBay](http://www.ebay.com) who loves all things JavaScript. I'm also a growing fan of the [Godot](https://godotengine.org/) game engine and [Rust](https://www.rust-lang.org/). I'm a husband and father.

You can reach me via email at `triptych (at gmail dot com)`. Or you can find me on Twitter @[triptych](https://twitter.com/triptych) .

* 🖊️ I am a technical writer on LeanPub -> [triptych](https://leanpub.com/u/triptych).
* 🐲 I am a fantasy writer on WattPad -> [triptych](https://www.wattpad.com/user/triptych). 
* ✨ I also write on Inkitt -> [triptych](https://www.inkitt.com/triptych).
* 💻 I am a coder on GitHub -> [triptych](https://github.com/triptych).
* 🐘 I am a friend on Mastodon -> [@triptych@whisperstorm.xyz
](https://whisperstorm.xyz/@triptych)
* 🕹️ I am a game developer on itch.io -> [triptych](https://triptych.itch.io)
* 🔑 My identity lives here on KeyBase -> [triptych](https://keybase.io/triptych)
* 🏺 My tip jar -> [triptych](https://ko-fi.com/triptych)
* 📜 My resume -> [triptych](https://gist.github.com/triptych/54cdf5112feeadb9ca91)
* 🏠 My site -> [Andreww.xyz](https://andreww.xyz/)
* ⚗️ My GumRoad -> [triptych](https://gumroad.com/triptych)
* 🧙‍♂️ My Right Brained Site -> [triptych.neocities.org](https://triptych.neocities.org)
* 🌌 My Blog -> [triptych.writeas.com](https://triptych.writeas.com)



(Thanks to [this article](https://brainfood.xyz/post/20190518-host-your-own-blog-in-1-hour/) for the inspiration for this site)

<a href='https://ko-fi.com/A3054BXO' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://az743702.vo.msecnd.net/cdn/kofi2.png?v=2' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>
