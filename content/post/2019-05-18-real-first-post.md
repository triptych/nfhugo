---
title: First real post!
date: 2019-05-18
subtitle: Starting something new
---

I just went through the process of setting up this Hugo based website where I can edit pages via gitlab, and have it post through Netlify.

I feel like I've 'leveled up' in my goals to finally have a static site. 

Big thanks to [this post](https://brainfood.xyz/post/20190518-host-your-own-blog-in-1-hour/) for showing me the way.